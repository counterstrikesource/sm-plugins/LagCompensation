# LagCompensation

## What is lag compensation?
- https://developer.valvesoftware.com/wiki/Source_Multiplayer_Networking#Lag_compensation
- https://developer.valvesoftware.com/wiki/Lag_compensation

Watch [this video](https://www.youtube.com/watch?v=6EwaW2iz4iA)

## Okay, so why this plugin?
The source engine has lag compensation for player entities ONLY.
Anything else that's moving around in the server is NOT lag compensated.
This plugin fixes this for the most part.

It provides accurate and cheap (no interpolation between two snapshots) lag compensation for trigger_{hurt,push,teleport} and func_physbox* entities which are parented to moving entities.
It does this automatically and avoids breaking

## How does it work?
The plugin remembers the last 32 (configurable) states of up to 256 (configurable) entities.
It keeps track if entities are actually moving and only compensates them if they are.
There are barely any performance implications of running this plugin, especially because it also optimizes how triggers work. If a trigger is lag compensated it will not run any physics simulation any more.

Usually a moving trigger would be actively scanning and touching any entity in its collision hull.
To make lag compensation possible this is blocked with the PhysHooks extension and now only players will be touching triggers.
Before a player is simulated, all (active/moving) entities are moved to where they are on their screen. This is done for each player.
At the end of the player simulation loop the compensated entities are restored to their server state.
After the entities have been simulated their parameters are recorded.

## Issues
Moving platforms/trains are not lag compensated. If a trigger is parented to it, it will be lag compensated however.
Imagine a thin platform with a kill trigger parented on the bottom of it. If a player stands on the platform and it moves up the trigger on the bottom will move through the platform, into the player and kill them.
In this case you'll have to modify the map with something like stripper-source and add the no-lagcomp flag to the trigger.

Only letting players touch triggers comes with some issues on CS:S however, if a player stops sending packets to the server they are not simulated at all any more. Hence they will not touch any triggers that move through them.
CS:GO seems to have a fix for this in built. For CS:S I've included a plugin AntiLagSwitch which forces player simulation with empty packets after the server didn't hear from them for 16 ticks.

Because source has no client side prediction of triggers and we can't cheat the laws of physics something else has to be delayed now: death.
You play with 300ms ping and seemingly jump over a laser, only to die after it has already passed you.
A smart player will obviously scream "fucking 0 ping botox you gamer-word" into the voice chat.
A not-so-smart player will realize they made a mistake and it's just the information of their death which had to travel for 300ms before reaching them.

3rd person and server demos view for players with lag compensation enabled will look weird, they will touch lasers on your screen but not die.
This makes sense of course because on their screen they've jumped over the laser, that's the whole point of lag compensation.
To get an accurate view of their screen the demo needs to be recorded in their client.

## Usage
Install the DHooks + Detours extension: https://forums.alliedmods.net/showpost.php?p=2588686&postcount=589

Install the PhysHooks extension: https://forums.alliedmods.net/showthread.php?p=2684809#post2684809

Install the LagCompensation.smx into your plugins/ folder and LagCompensation.games.txt into your gamedata/ folder.

If you want to avoid a trigger (or physbox) from being lag compensated by this plugin then you can add the value 1073741824 (1 << 30) to the spawnflags of the entity.

Players can choose to effectively this plugin by using the commands: !lagcomp or !0ping
(effectively: the plugin will always take the newest entity snapshot for those players, it will assume the player actually has 0 ping)

## Thanks to:
All players and staff of UNLOZE Zombie Escape server.

While developing this plugin there have been many bugs and weird things happening for weeks.

Thanks for not pulling the plug and letting me continue working on this until all issues have been fixed.

Special thanks to xen for a lot of help with testing and investigating issues.

POSSESSION and ZombiEden for testing the CS:GO Win32 version and helping with windows signatures.

Obviously the SourceMod project, without SourceMod most of our mods would be incredibly harder or outright not possible.

Dr!fter and Pyschonic for DHooks + Detours.

The leaked 2007 engine source code provided insight into the depths of the engine, PhysHooks makes heavy use of this.

## Video demonstration
Don't jump as early as [this guy](https://www.youtube.com/watch?v=ubZtbI0xCGE)

In the past you had to jump lasers very early depending on your ping and lerp, because there was no lag compensation.
Now you'll just die because you fall into the laser.

## Git repository
https://git.botox.bz/CSSZombieEscape/sm-plugin-LagCompensation

## Donations
CS:GO Windows source code and builds have been released publicly after I've received donations in total of 200€.

Supporting Windows requires a lot more effort, debugging it even more.
It's also really stupid to run a Windows server when Linux causes a lot less trouble.

https://paypal.me/BotoXbz

- 50€ - Net4All.ru
- 20€ - JAZE
- 60€ - GFLClan.com
- 65€ - Kxnrl
- 70€ - Mapeadores.com